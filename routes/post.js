'use strict'
const express = require('express');
const isAuth = require('../middlewares/isAuth');
const postController = require('../controllers/post');
const router = express.Router();

router.get('/post', postController.getPosts);
router.get('/post/:id', postController.getPostById);
router.post('/post', isAuth, postController.createPost);
router.patch('/post/:id', isAuth, postController.updatePost);
router.delete('/post/:id', isAuth, postController.deletePost);

module.exports = router;