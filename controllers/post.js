'use strict'
const { Post, User } = require('../models')
const client = require('../utils/redis');
const { promisify } = require('util');

const GET_ASYNC = promisify(client.get).bind(client);
const SETEX_ASYNC = promisify(client.setex).bind(client);

exports.getPosts = async (req, res, next) => {
    const currentPage = req.query.page || 1;
    const perPage = 6;
    try {
        const reply = await GET_ASYNC("posts");
        if (reply) {
            const posts = JSON.parse(reply);
            return res.status(200).json({ posts: posts });
        }
        const { count, rows } = await Post.findAndCountAll({ offset: (currentPage - 1) * perPage, limit: perPage });
        const posts = JSON.stringify(rows);
        await SETEX_ASYNC("posts", 25, posts);
        res.status(200).json({ posts: rows });
    } catch (err) {
        if (!err.statusCode) {
            err.statusCode = 500;
        }
        next(err);
    }
};

exports.getPostById = async (req, res, next) => {
    const { postId } = req.params.postId;
    try {
        const post = await Post.findByPk(postId);
        if (!post) {
            const error = new Error("Post doesn't found");
            error.statusCode = 404;
            throw error;
        }

        res.status(200).json({ post: post });

    } catch (err) {
        if (!err.statusCode) {
            err.statusCode = 500;
        }
        next(err);
    }
}

exports.createPost = async (req, res, next) => {
    const { title, description } = req.body;
    const { userId } = req;

    console.log(userId);

    try {
        const userExist = await User.findByPk(userId);
        if (!userExist) {
            const error = new Error("User invalid");
            error.statusCode = 401;
            throw error;
        }
        const post = await Post.create({ title: title, description: description, userId: userExist.id });
        res.status(201).json({ message: "Post created succesfully", post: { title: post.title, description: post.description } });
    } catch (err) {
        if (!err.statusCode) {
            err.statusCode = 500;
        }
        next(err);
    }
};

exports.updatePost = async (req, res, next) => {
    const postId = req.params.postId;
    const { title, description } = req.body;
    try {
        const post = await Post.findByPk(postId);
        if (!post) {
            const error = new Error("Post doesn't exist");
            error.statusCode = 404;
            throw error;
        }
        post.title = title;
        post.description = description;
        await post.save();
        res.status(201).json({ message: "Post updated succesfully!", post: { title: post.title, description: post.description } });
    } catch (err) {
        if (!err.statusCode) {
            err.statusCode = 500;
        }
        next(err);
    }

};

exports.deletePost = async (req, res, next) => {
    const postId = req.params.id;
    try {
        const post = await Post.findByPk(postId);
        if (!post) {
            const error = new Error("Post doesn't exist.");
            error.statusCode = 404;
            throw error;
        }
        await post.destroy();
        res.status(200).json({ message: "Post deleted succesfully." });
    } catch (err) {
        if (!err.statusCode) {
            err.statusCode = 500;
        }
        next(err);
    }

};