`use strict`
const express = require('express');
const app = express();
const path = require('path');
const dotenv = require('dotenv');
const multer = require('multer');


dotenv.config({ path: `.env.${app.get('env')}` });

const PORT = process.env.PORT || 3000;
const ROUTE = process.env.ROUTE;
const VERSION = process.env.VERSION;



const fileStorage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, 'images');
    },
    filename: (req, file, cb) => {
        cb(null, `${file.originalname}-${new Date().toISOString()}`);
    }
});

const fileFilter = (req, file, cb) => {
    if (file.mimetype === 'image/png' || file.mimetype === 'image/jpeg' || file.mimetype === 'image/jpg') {
        cb(null, true);
    } else {
        cb(null, false);
    }
};

const authRoute = require('./routes/auth');
const userRoute = require('./routes/user');
const postRoute = require('./routes/post');


app.use(express.json());
app.use(multer({ storage: fileStorage, fileFilter: fileFilter }).single('image'));
app.use('/images', express.static(path.join(__dirname, 'images')));
app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,PUT,PATCH,DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'Content-Type,Authorization');
    next();
});


app.use(ROUTE + VERSION, authRoute);
app.use(ROUTE + VERSION, userRoute);
app.use(ROUTE + VERSION, postRoute);

app.use((error, req, res, next) => {
    const errorStatus = error.statusCode || 500;
    const errorMessage = error.message;
    const errorData = error.data;
    res.status(errorStatus).json({ message: errorMessage, data: errorData });
});


const server = app.listen(PORT, () => {
    console.log(`API listening on port ${PORT}.`);
});


module.exports = { app, server };