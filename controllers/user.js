'use strict'
const { User } = require('../models');
const client = require('../utils/redis');
const { promisify } = require('util');

const GET_ASYNC = promisify(client.get).bind(client);
const SETEX_ASYNC = promisify(client.setex).bind(client);

exports.getUsers = async (req, res, next) => {
    const currentPage = req.query.page || 1;
    const perPage = 4;

    try {
        const reply = await GET_ASYNC("users");
        if (reply) {
            const users = JSON.parse(reply);
            return res.status(200).json({ users: users });
        }
        const { count, rows } = await User.findAndCountAll({ offset: (currentPage - 1) * perPage, limit: perPage });
        const users = rows.map((user) => {
            delete user.dataValues.password;
            return user;
        });
        await SETEX_ASYNC("users", 10, JSON.stringify(users));

        res.status(200).json({ users: users });
    } catch (err) {
        if (!err.statusCode) {
            err.statusCode = 500;
        }
        next(err);
    }
};

exports.updateUser = async (req, res, next) => {
    const { userId } = req;
    const { name, email } = req.body;
    try {
        const user = await User.findByPk(userId);
        if (!user) {
            const error = new Error("User doesn't exist.");
            error.statusCode = 400;
            throw error;
        }
        user.name = name;
        user.email = email;
        await user.save();
        res.status(201).json({ user: { name: user.name, email: user.email } });
    } catch (err) {
        if (!err.statusCode) {
            err.statusCode = 500;
        }
        next(err);
    }

};

exports.deleteUser = async (req, res, next) => {
    const { userId } = req;
    try {
        const user = await User.findByPk(userId);
        if (!user) {
            const error = new Error("User doesn't exist.");
            error.statusCode = 500;
            throw error;
        }
        await user.destroy();
        res.status(200).json({ message: "user deleted." });
    } catch (err) {
        if (!err.statusCode) {
            err.statusCode = 500;
        }
        next(err);
    }
};