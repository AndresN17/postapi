'use strict'
const supertest = require('supertest');
const { app, server } = require('../index');
const client = require('../utils/redis');
const db = require('../utils/database');

const api = supertest(app);
const ROUTE = process.env.ROUTE;
const VERSION = process.env.VERSION;

const user = {
    email: "andresnoboa17@hotmail.com",
    name: "Andres",
    password: "askajka",
    enable: true
};

const { User } = require('../models');

beforeEach(async () => {
    const oldUser = await User.findOne({ where: { email: "andresnoboa17@hotmail.com" } });
    if (oldUser) {
        await oldUser.destroy();
    }
    await User.create(user);
});


test("users are returned as JSON", async () => {
    await api
        .get(`${ROUTE}${VERSION}/user`)
        .expect(200)
        .expect('Content-Type', /application\/json/);
});

test("There is one User", async () => {
    const response = await api.get(`${ROUTE}${VERSION}/user`);
    expect(response.body.users).toHaveLength(1);

});

afterAll(async () => {
    server.close();
    await db.close();
    client.end(true);
});