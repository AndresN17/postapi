'use strict'
const express = require('express');
const userController = require('../controllers/user');
const router = express.Router();


router.get('/user', userController.getUsers);
router.patch('/user/:id', userController.updateUser);

module.exports = router;