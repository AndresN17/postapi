# Post REST API
A simple POST API that let you make entries about your day or anything.

# To Do
- [x] Authentication
- [x] Users Endpoint
- [x] Post Endpoint
- [x] Error Middleware
- [x] Adding cache with Redis
- [ ] Adding logging
- [ ] Testing Endpoints
- [ ] Add email messages